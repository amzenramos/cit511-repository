from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse, request
from .models import Question
from .forms import QuestionForm
from datetime import datetime
from django.contrib import messages

# Create your views here.
def index(request):
   latest_question_list = Question.objects.order_by('-pub_date')[:5]
   context = {
      'latest_question_list':latest_question_list,
   }
   return render(request, 'polls/index.html', context)
   # return HttpResponse(" <h1> Hello World, you are on the polls index page. </h1>")

def detail(request, question_id):
   question = get_object_or_404(Question, pk=question_id)
   context = {
      'question':question,
   }
   return render(request, 'polls/detail.html', context)

def questionForm(request):
      if request.method == 'POST':
          form = QuestionForm(request.POST)
          if form.is_valid():
              new_question = form.save(commit=False) 
              new_question.pub_date = datetime.now()
              new_question.save()
              messages.info(request, 'you have successfully added the question.')
              return redirect('question_form')
      else:
         form = QuestionForm()
         context = {
            'form':form,
         }
      return render(request, 'polls/addquestion.html', context)

def updateQuestion(request, question_id):
   question = get_object_or_404(Question, pk=question_id)
   form = QuestionForm(instance = question)
   if request.method == 'POST':
      form = QuestionForm(request.POST, instance=question)
      if form.is_valid():
         form.save()
         return redirect('index')

   context = {
      'form':form
   }
   return render(request, 'polls/editdetail.html', context)

def deleteQuestion(request, question_id):
   question = get_object_or_404(Question, pk=question_id)
   question.delete()
   return redirect('index')