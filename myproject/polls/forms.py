from django.forms import ModelForm
from django import forms
from .models import Question

class QuestionForm(ModelForm):
   class Meta:
      model = Question
      fields = ['questions_text',]