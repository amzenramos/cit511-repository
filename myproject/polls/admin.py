from django.contrib import admin
from .models import Question
# Register your models here.

class QuestionAdminLook(admin.ModelAdmin):
    list_display = ['pk','questions_text','pub_date']

admin.site.register(Question, QuestionAdminLook)