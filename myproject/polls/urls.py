from django.urls import path
from . import views

urlpatterns = [
     path('', views.index, name='index'),
     path('detail/<int:question_id>', views.detail, name='detail'),
     path('question_form', views.questionForm, name='question_form'),
     path('update_question/<int:question_id>', views.updateQuestion, name='update_question'),
     path('delete_question/<int:question_id>', views.deleteQuestion, name='delete_question'),
]